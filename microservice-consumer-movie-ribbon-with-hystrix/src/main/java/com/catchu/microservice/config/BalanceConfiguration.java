package com.catchu.microservice.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 刘俊重
 * @Description 调用配置,配置调用者调用服务者的调用策略
 * @date 13:56
 */
@Configuration
public class BalanceConfiguration {

    /**
     * @Description 定义随机调用的负载均衡算法
     * @Author 刘俊重
     * @Date 2017/11/2
     */
    @Bean
    public IRule ribbonRule(){
        return new RandomRule();
    }
}
