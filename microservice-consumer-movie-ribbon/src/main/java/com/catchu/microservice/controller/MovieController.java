package com.catchu.microservice.controller;

import com.catchu.microservice.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author 刘俊重
 * @Description 电影服务控制层
 * @date 18:29
 */
@RestController
public class MovieController {

    /**
     * @Description restTemplate名字与MicroserviceSimpleConsumerMovieApplication里面@bean创建的对象名要一样
     * @Author 刘俊重
     * @Date 2017/10/30
     */
    @Autowired
    private RestTemplate restTemplate;


    /**
     * @Description 使用restTemplate对象调用服务者——用户模块
     * @Author 刘俊重
     * @Date 2017/10/30
     */
    @GetMapping("/simple/{id}")
    public User findUserById(@PathVariable Long id){
        return restTemplate.getForObject("http://microservice-provider-user/simple/"+id, User.class);
    }
}
