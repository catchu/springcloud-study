package com.catchu.me.feign;

import com.catchu.me.entity.User;
import org.springframework.stereotype.Component;

/**
 * @author 刘俊重
 * @Description
 * @date 19:27
 */
@Component
public class UserFeignFallBack implements UserFeign{

    @Override
    public User findUserbyId(Long id) {
        User user = new User();
        user.setId(-1L);
        user.setUsername("服务者出错了");
        return user;
    }
}
