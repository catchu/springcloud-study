package com.catchu.me.feign;

import com.catchu.me.entity.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @Description 用户调用Feign
 * @Author 刘俊重
 * @Date 2017/11/8
 */
@FeignClient(name = "microservice-provider-user", fallback = UserFeignFallBack.class)
public interface UserFeign {

    @RequestMapping(value = "/simple/{id}",method = RequestMethod.GET)
    public User findUserbyId(@PathVariable("id") Long id);
}
