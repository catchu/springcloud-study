package com.catchu.microservice.controller;

import com.catchu.microservice.dao.UserDao;
import com.catchu.microservice.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 刘俊重
 * @Description
 * @date 18:09
 */
@RestController
public class UserController {

    @Autowired
    private UserDao userDao;

    @GetMapping("/simple/{id}")
    public User findUserbyId(@PathVariable Long id){
        return userDao.findOne(id);
    }
}
