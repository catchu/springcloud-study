package com.catchu.microservice.dao;

import com.catchu.microservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 刘俊重
 * @Description 用户数据访问层
 * @date 18:06
 */
@Repository
public interface UserDao extends JpaRepository<User,Long>{

}
