package com.catchu.me.feign;

import com.catchu.me.entity.User;
import feign.hystrix.FallbackFactory;

/**
 * @author 刘俊重
 * @Description
 * @date 19:15
 */
public class UserFeignHystrixFactory implements FallbackFactory<UserFeign> {

    @Override
    public UserFeign create(Throwable cause) {
        return new UserFeignWithFactory() {
            @Override
            public User findUserbyId(Long id) {
                User user = new User();
               user.setId(-2L);
                return user;
            }
        };
    }
}
