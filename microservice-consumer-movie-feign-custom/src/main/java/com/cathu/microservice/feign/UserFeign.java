package com.cathu.microservice.feign;

import com.cathu.microservice.entity.User;
import com.config.CustomFeignConfiguration;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
/**
 * @Description 用户调用Feign
 * @Author 刘俊重
 * @Date 2017/11/8
 */
@FeignClient(name="microservice-provider-user",configuration = CustomFeignConfiguration.class)
public interface UserFeign {

    @RequestLine("GET /simple/{id}")
    public User findUserbyId(@Param("id") Long id);
}
