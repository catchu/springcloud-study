package com.cathu.microservice.controller;

import com.cathu.microservice.entity.User;
import com.cathu.microservice.feign.UserFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 刘俊重
 * @Description 电影服务控制层
 * @date 18:29
 */
@RestController
public class MovieController {

    @Autowired
    private UserFeign userFeign;

    /**
     * @Description 使用restTemplate对象调用服务者——用户模块
     * @Author 刘俊重
     * @Date 2017/10/30
     */
    @GetMapping("/simple/{id}")
    public User findUserById(@PathVariable Long id){
        return userFeign.findUserbyId(id);
    }
}
